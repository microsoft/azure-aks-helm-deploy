# Bitbucket Pipelines Pipe: Azure AKS Helm Deploy

A pipe that uses a [Helm](https://helm.sh/) to interact with a Kubernetes cluster running on [Azure Kubernetes Service](https://docs.microsoft.com/en-us/azure/aks/)

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: microsoft/azure-aks-helm-deploy:1.0.2
    variables:
      AZURE_APP_ID: '<string>'
      AZURE_PASSWORD: '<string>'
      AZURE_TENANT_ID: '<string>'
      AZURE_AKS_NAME: '<string>'
      AZURE_RESOURCE_GROUP: '<string>'
      # HELM_VERSION: '<string>'
      # HELM_RELEASE_NAME: '<string>'
      # HELM_CHART_NAME: '<string>'
      # HELM_CHART_DIR: '<string>'
      # HELM_COMMAND: '<string>'
      # HELM_COMMAND_WAIT: '<string>'
      # HELM_UPGRADE_INSTALL_IF_NOT_PRESENT: '<string>'
      # DEBUG: '<boolean>' # Optional
```

## Variables

| Variable                     | Usage                                                       |
| ---------------------------- | ----------------------------------------------------------- |
| AZURE_APP_ID (*)        | The app ID, URL or name associated with the service principal required for login. |
| AZURE_PASSWORD (*)      | Credentials like the service principal password, or path to certificate required for login. |
| AZURE_TENANT_ID  (*)    | The AAD tenant required for login with the service principal. |
| AZURE_AKS_NAME (*)      | Name of the AKS management service to connect to.
| AZURE_RESOURCE_GROUP (*) | Name of the resource group that the AKS management service is deployed to.  |
| HELM_VERSION                 | The version of HELM in your cluster Default: `latest`. |
| HELM_RELEASE_NAME            | The name of your deployed release |
| HELM_CHART_NAME              | The name of the helm chart to deploy "repo/chart" |
| HELM_CHART_DIR               | The local directory for your helm chart |
| HELM_COMMAND                 | The helm command to run. e.g. "upgrade" |
| HELM_COMMAND_WAIT            | Wait for the helm command to finish executing. Default: `true`. |
| HELM_UPGRADE_INSTALL_IF_NOT_PRESENT | Forces an install if using "upgrade" command. Default: `true`. |
| DEBUG                        | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Prerequisites

You will need to configure required Azure resources before running the pipe. The easiest way to do it is by using the Azure cli. You can either [install the Azure cli](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest) on your local machine, or you can use the [Azure Cloud Shell](https://docs.microsoft.com/en-us/azure/cloud-shell/overview) provided by the Azure Portal in a browser.

### Service principal

You will need a service principal with sufficient access to create an AKS service, or update an existing AKS service. To create a service principal using the Azure CLI, execute the following command in a bash shell:

```sh
az ad sp create-for-rbac --name MyAKSServicePrincipal
```

Get the resource id for AKS:

```sh
az aks show -g MyResourceGroup -n myakscluster --query id 
"/subscriptions/00000000-0000-0000-0000-000000000000/resourcegroups/MyResourceGroup/providers/Microsoft.ContainerService/managedClusters/myakscluster"
```

Add the role assignment "Azure Kubernetes Service Cluster User Role" to the service principal and scope to AKS:

```sh
az role assignment create --assignee 00000000-0000-0000-0000-000000000000 --role "Azure Kubernetes Service Cluster User Role" --scope "/subscriptions/00000000-0000-0000-0000-000000000000/resourcegroups/MyResourceGroup/providers/Microsoft.ContainerService/managedClusters/myakscluster"
```

Test the service principal login:

```sh
az login --service-principal --username 00000000-0000-0000-0000-000000000000 --password 00000000-0000-0000-0000-000000000000 --tenant 00000000-0000-0000-0000-000000000000
```

Refer to the following documentation for more detail:

* [Service principals with Azure Kubernetes Service (AKS)](https://docs.microsoft.com/en-us/azure/aks/kubernetes-service-principal)
* [Create an Azure service principal with Azure CLI](https://docs.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli)

### AKS Instance

Using the service principal credentials obtained in the previous step, you can use the following commands to create an AKS instance in a bash shell:

```bash
az login --service-principal --username ${AZURE_APP_ID}  --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}

az group create --name ${AZURE_RESOURCE_GROUP} --location australiaeast

az aks create \
--resource-group ${AZURE_RESOURCE_GROUP} \
--name ${AZURE_AKS_NAME} \
--node-count 1 \
--enable-addons monitoring \
--service-principal ${AZURE_APP_ID} \
--client-secret ${AZURE_PASSWORD} \
--generate-ssh-keys
```

Next you need to configure the AKS instance.
Start by installing kubectl and helm if you don't already have it installed.

```bash
az aks install-cli
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get > get_helm.sh
chmod 700 get_helm.sh
./get_helm.sh
```

Get the credentials for kubectl to connect to your cluster.

```bash  
az aks get-credentials --resource-group ${AZURE_RESOURCE_GROUP} --name ${AZURE_AKS_NAME}
```

Now you need to create a service account for tiller, and a role binding. Create a file named `helm-rbac.yaml` and copy in the following YAML:

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: tiller
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: tiller
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: tiller
    namespace: kube-system
```

Apply this configuration to your cluster.

```bash
kubectl apply -f ${BATS_TEST_DIRNAME}/helm-rbac.yaml
```

Install tiller into your cluster.

```bash
helm init --service-account tiller
```

Refer to the following documentation for more detail:

* [Install applications with Helm in Azure Kubernetes Service (AKS)](https://docs.microsoft.com/en-us/azure/aks/kubernetes-helm)

## Examples

Basic example using helm chart in online repo:

```yaml
script:
  - pipe: microsoft/azure-aks-helm-deploy:1.0.2
    variables:
      AZURE_APP_ID: $AZURE_APP_ID
      AZURE_PASSWORD: $AZURE_PASSWORD
      AZURE_TENANT_ID: $AZURE_TENANT_ID
      AZURE_AKS_NAME: $AZURE_AKS_NAME
      AZURE_RESOURCE_GROUP: $AZURE_RESOURCE_GROUP
      HELM_VERSION: 'v2.10.0'
      HELM_RELEASE_NAME: 'myRedis'
      HELM_COMMAND: 'upgrade'
      HELM_UPGRADE_INSTALL_IF_NOT_PRESENT: 'true'
      HELM_CHART_NAME: 'stable/redis'
```

Advanced example using helm chart in local source code:

```yaml
script:
  - pipe: microsoft/azure-aks-helm-deploy:1.0.2
    variables:
      AZURE_APP_ID: $AZURE_APP_ID
      AZURE_PASSWORD: $AZURE_PASSWORD
      AZURE_TENANT_ID: $AZURE_TENANT_ID
      AZURE_AKS_NAME: $AZURE_AKS_NAME
      AZURE_RESOURCE_GROUP: $AZURE_RESOURCE_GROUP
      HELM_VERSION: 'v2.10.0'
      HELM_RELEASE_NAME: 'myCustomRedis'
      HELM_COMMAND: 'upgrade'
      HELM_UPGRADE_INSTALL_IF_NOT_PRESENT: 'true'
      HELM_CHART_DIR: './helm-charts/redisv1'
```

## Support

This sample is provided "as is" and is not supported. Likewise, no commitments are made as to its longevity or maintenance. To discuss this sample with other users, please visit the Azure DevOps Services section of the Microsoft Developer Community: https://developercommunity.visualstudio.com/spaces/21/index.html.
