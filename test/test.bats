#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/azure-aks-helm-deploy"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:0.1.0 .

  echo "# Create testing resources" >&3

  AZURE_RESOURCE_GROUP="bitbucket-aks-helm-deploy-test-${BITBUCKET_BUILD_NUMBER}"
  AZURE_AKS_NAME="bitbucket-aks-helm-deploy-test-${BITBUCKET_BUILD_NUMBER}"

  echo "# login" >&3
  az login --service-principal --username ${AZURE_APP_ID} --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}
    
  echo "# creating resource group ${AZURE_RESOURCE_GROUP}" >&3
  az group create --name ${AZURE_RESOURCE_GROUP} --location westus2
  
  echo "# creating AKS cluster ${AZURE_AKS_NAME}" >&3  
  az aks create \
    --resource-group ${AZURE_RESOURCE_GROUP} \
    --name ${AZURE_AKS_NAME} \
    --node-count 1 \
    --enable-addons monitoring \
    --service-principal ${AZURE_APP_ID} \
    --client-secret ${AZURE_PASSWORD} \
    --generate-ssh-keys

  echo "# install kubectl and helm" >&3
  az aks install-cli
  curl https://raw.githubusercontent.com/helm/helm/master/scripts/get > get_helm.sh
  chmod 700 get_helm.sh
  ./get_helm.sh
  
  az aks get-credentials --resource-group ${AZURE_RESOURCE_GROUP} --name ${AZURE_AKS_NAME}

  kubectl apply -f ${BATS_TEST_DIRNAME}/helm-rbac.yaml

  helm init --service-account tiller
    
  # Give the tiller pod some time to come online
  sleep 30s

  echo "# test setup complete " >&3  
}

teardown() {
    echo "# teardown - deleting resource group ${AZURE_RESOURCE_GROUP}" >&3
    az group delete -n ${AZURE_RESOURCE_GROUP} -y --no-wait
}

@test "Install Redis into AKS using Helm" {
    run docker run \
        -e AZURE_APP_ID=${AZURE_APP_ID} \
        -e AZURE_PASSWORD=${AZURE_PASSWORD} \
        -e AZURE_TENANT_ID=${AZURE_TENANT_ID} \
        -e AZURE_AKS_NAME=${AZURE_AKS_NAME} \
        -e AZURE_RESOURCE_GROUP=${AZURE_RESOURCE_GROUP} \
        -e HELM_VERSION="v2.10.0" \
        -e HELM_RELEASE_NAME="redis" \
        -e HELM_COMMAND="upgrade" \
        -e HELM_CHART_NAME="stable/redis" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:0.1.0

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

